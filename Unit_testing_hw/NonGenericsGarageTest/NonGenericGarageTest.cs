﻿using System;
using GarageWithOutGenerics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GarageWithOutGenericsTest
{
    [TestClass]
    public class GarageTest
    {
        #region Private Memeber
        private IGarage m_car;
        private Car car = new Car("Honda", false, true);
        #endregion

        #region Test Initializer
        [TestInitialize]
        public void Initializer()
        {
            m_car = new Garage(TestData.cars);
        }
        #endregion

        #region AddCar 
        [TestMethod]
        [ExpectedException(typeof(CarAleadyHereException))]
        public void AddCar_AlreadyHereException()
        {
            Car car = new Car("Toyota", true, false);
            m_car.AddCar(car);
        }

        [TestMethod]
        [ExpectedException(typeof(WeDoNotFixTotalLostException))]
        public void AddCar_Total_Lost_false_Exception()
        {
            Car car = new Car("Toyota", true, false);
            Assert.AreEqual(car.TotalLost, true);
        }

        [TestMethod]
        [ExpectedException(typeof(WrongGarageException))]
        public void AddCar_Not_In_Garage_List_Exception()
        {
            Car car = new Car("Audi", false, true);
            Assert.IsFalse(TestData.cars.Contains(car.Brand));
            m_car.AddCar(car);
            m_car.FixCar(car);
            m_car.TakeOutCar(car);
        }

        //Works on three functions AddCar, FixCar, TakeOutCar
        [TestMethod]
        [ExpectedException(typeof(CarNullException))]
        public void Add_Fix_TakeOut_Car_NULL()
        {
            TestData.Add_Fix_TakeOut_Car_NULL = car;
            Assert.IsNull(TestData.Add_Fix_TakeOut_Car_NULL);
            m_car.AddCar(TestData.Add_Fix_TakeOut_Car_NULL);
            m_car.FixCar(TestData.Add_Fix_TakeOut_Car_NULL);
            m_car.TakeOutCar(TestData.Add_Fix_TakeOut_Car_NULL);

        }

        //Works for AddCar function and FixCar Function
        [TestMethod]
        [ExpectedException(typeof(RepairMismatchException))]
        public void Add_Fix_No_Need_Repair()
        {
            Car car = new Car("Toyota", false, true);
            Assert.IsFalse(car.NeedsRepair);
        }
        #endregion

        #region TakeOutCar and FixCar Test Throws Exceptions
        [TestMethod]
        [ExpectedException(typeof(CarNotInGarageException))]
        public void TakeOutCar_FixCar_Car_Not_In_Garage()
        {
            Assert.IsFalse(TestData.cars.Contains(car.Brand));
            m_car.FixCar(car);
            m_car.TakeOutCar(car);
        }

        [TestMethod]
        [ExpectedException(typeof(CarNotReady))]
        public void TakeOutCar_Not_Ready()
        {
            Assert.IsTrue(car.NeedsRepair);
            m_car.FixCar(car);
        }
        #endregion
    }
}

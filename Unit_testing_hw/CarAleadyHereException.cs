﻿using System;
using System.Runtime.Serialization;

namespace GarageWithOutGenerics
{
    [Serializable]
    public class CarAleadyHereException : Exception
    {
        public CarAleadyHereException()
        {
        }

        public CarAleadyHereException(string message) : base(message)
        {
        }

        public CarAleadyHereException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CarAleadyHereException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
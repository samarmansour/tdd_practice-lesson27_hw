﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageWithOutGenerics
{
    public class Garage : IGarage
    {
        private List<Car> cars = new List<Car>();
        private List<string> carTypes;

        public Garage(List<string> carTypes)
        {
            this.carTypes = carTypes;
        }

        public void AddCar(Car car)
        {
            if (carTypes.Contains(car.Brand))
                throw new CarAleadyHereException($"{car.Brand} Already in the garage!");
            if (car.TotalLost == true)
                throw new WeDoNotFixTotalLostException($"Sorry! we can't Fix it, Total Lost is {car.TotalLost}");
            if (!carTypes.Contains(car.Brand))
                throw new WrongGarageException($"Sorry! we Can't fix {car.Brand} type of cars");
            if (car.Brand is null)
                throw new CarNullException("Invalid car!!");
            if (car.NeedsRepair == false)
                throw new RepairMismatchException("No need to repair!");
            carTypes.Add(car.Brand);
        }

        public void FixCar(Car car)
        {
            if (car.Brand is null)
                throw new CarNullException("Invalid car!!");
            if (cars.Contains(car))
                throw new CarNotInGarageException($"{car.Brand} Not in the garage");
            if (car.NeedsRepair == true)
                throw new CarNotReady($"Still Fixing {car.Brand}");
            carTypes.Remove(car.Brand);
        }

        public void TakeOutCar(Car car)
        {
            if (car.Brand is null)
                throw new CarNullException("Invalid car!!");
            if (carTypes.Contains(car.Brand))
                throw new CarNotInGarageException($"{car.Brand} Not in the garage");
            if (car.NeedsRepair == false)
                throw new RepairMismatchException("No need to repair!");
            Console.WriteLine(!car.NeedsRepair);

        }
    }
}

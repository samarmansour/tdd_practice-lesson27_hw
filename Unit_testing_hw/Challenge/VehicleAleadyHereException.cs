﻿using System;
using System.Runtime.Serialization;

namespace Challenge
{
    [Serializable]
    public class VehicleAleadyHereException : Exception
    {
        public VehicleAleadyHereException()
        {
        }

        public VehicleAleadyHereException(string message) : base(message)
        {
        }

        public VehicleAleadyHereException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected VehicleAleadyHereException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
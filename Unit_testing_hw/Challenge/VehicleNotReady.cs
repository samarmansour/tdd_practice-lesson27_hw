﻿using System;
using System.Runtime.Serialization;

namespace Challenge
{
    [Serializable]
    internal class VehicleNotReady : Exception
    {
        public VehicleNotReady()
        {
        }

        public VehicleNotReady(string message) : base(message)
        {
        }

        public VehicleNotReady(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected VehicleNotReady(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
﻿using System;
using System.Runtime.Serialization;

namespace Challenge
{
    [Serializable]
    internal class VehicleNotInGarageException : Exception
    {
        public VehicleNotInGarageException()
        {
        }

        public VehicleNotInGarageException(string message) : base(message)
        {
        }

        public VehicleNotInGarageException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected VehicleNotInGarageException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
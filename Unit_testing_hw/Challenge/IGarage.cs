﻿namespace GarageWithOutGenerics
{
    public interface IGarage<T>
    {
        void AddVehicle(T vehicle);
        void TakeOutVehicle(T vehicle);
        void FixVehicle(T vehicle);

    }
}
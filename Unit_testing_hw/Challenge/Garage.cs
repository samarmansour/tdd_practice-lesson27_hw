﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageWithOutGenerics
{
    public class Garage<T> : IGarage<T>, IFixTotalLost<T> where T : Bike
    {
        private List<T> vehicles = new List<T>();
        private List<string> vehicleTypes;

        public Garage(List<string> vehicleTypes)
        {
            this.vehicleTypes = vehicleTypes;
        }

        public bool TotalLost => true;

        public void AddVehicle(T vehicle) 
        {
            if (vehicleTypes.Contains(vehicle.Brand))
                throw new VehicleAleadyHereException($"{vehicle.Brand} Already in the garage!");
            if (vehicle.TotalLost == true)
                throw new WeDoNotFixTotalLostException($"Sorry! we can't Fix it, Total Lost is {vehicle.TotalLost}");
            if (!vehicleTypes.Contains(vehicle.Brand))
                throw new WrongGarageException($"Sorry! we Can't fix {vehicle.Brand} type of cars");
            if (vehicle.Brand is null)
                throw new VehicleNullException("Invalid vehicle!!");
            if (vehicle.NeedsRepair == false)
                throw new RepairMismatchException("No need to repair!");
            vehicleTypes.Add(vehicle.Brand);
        }

        public void FixVehicle(T vehicle)
        {
            if (vehicle.Brand is null)
                throw new VehicleNullException("Invalid car!!");
            if (vehicles.Contains(vehicle))
                throw new VehicleNotInGarageException($"{vehicle.Brand} Not in the garage");
            if (vehicle.NeedsRepair == true)
                throw new VehicleNotReady($"Still Fixing {vehicle.Brand}");
            vehicleTypes.Remove(vehicle.Brand);
        }

        public void TakeOutVehicle(T vehicle)
        {
            if (vehicle.Brand is null)
                throw new VehicleNullException("Invalid car!!");
            if (vehicleTypes.Contains(vehicle.Brand))
                throw new VehicleNotInGarageException($"{vehicle.Brand} Not in the garage");
            if (vehicle.NeedsRepair == false)
                throw new RepairMismatchException("No need to repair!");
            Console.WriteLine(!vehicle.NeedsRepair);

        }
    }
}

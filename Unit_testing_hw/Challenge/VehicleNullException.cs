﻿using System;
using System.Runtime.Serialization;

namespace Challenge
{
    [Serializable]
    public class VehicleNullException : Exception
    {
        public VehicleNullException()
        {
        }

        public VehicleNullException(string message) : base(message)
        {
        }

        public VehicleNullException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected VehicleNullException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageWithOutGenerics
{
    public interface IFixTotalLost<T>
    {
        bool TotalLost { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageWithOutGenerics
{
    public class Bike
    {
        public string Brand { get; private set; }
        public bool TotalLost { get; private set; }
        public bool NeedsRepair { get; private set; }
        public float EngineVolume { get; private set; }

        public Bike(string brand, bool totalLost, bool needsRepair)
        {
            Brand = brand;
            if (!totalLost && needsRepair)
                throw new RepairMismatchException($"Exception has been found!\nTotal Lost: {totalLost} Need Repair: {needsRepair}");
            TotalLost = totalLost;
            NeedsRepair = needsRepair;
        }

        public override string ToString()
        {
            return $"{base.ToString()}:   Brand: {Brand}  Total Lost: {TotalLost}  Needs Repair: {NeedsRepair}";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmTest
{
    static class TestData
    {
        internal const float ATM_withdraw_amount = 100f;
        internal const float ATM_withdraw_amount_return_0= 1000f;
        internal const float ATM_withdraw_amount_return_number= 50f;
        internal const float ATM_deposit_amount = 1000f;
        internal const float ATM_deposit_amount_negative = -50f;
        internal const string ATM_enterPincode_string = "12453";


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM
{
    public interface IMyAccount
    {
        bool EnterPincode(string pincode);
        float Withdraw(float amount);
        bool Deposit(float amount);

    }
}

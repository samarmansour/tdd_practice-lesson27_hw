﻿using System;
using System.Security.Permissions;
using ATM;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AtmTest
{
    [TestClass]
    public class MyAccountTest
    {
        private IMyAccount myAccount = new MyAccount();

        [TestMethod]
        [ExpectedException(typeof(SecurityException))]
        public void MyAccount_Despoit_no_EnterPincode()
        {
            myAccount.Deposit(TestData.ATM_deposit_amount);
            Assert.IsFalse(myAccount.EnterPincode(TestData.ATM_enterPincode_string));
        }

        [TestMethod]
        [ExpectedException(typeof(SecurityException))]
        public void MyAccount_Withdraw_no_EnterPincode()
        {
            myAccount.Withdraw(TestData.ATM_deposit_amount);
            Assert.IsFalse(myAccount.EnterPincode(TestData.ATM_enterPincode_string));
        }

        [TestMethod]
        [ExpectedException(typeof(SecurityException))]
        public void MyAccount_EnterPincode_Deposite_positive_return_true()
        {
            Assert.IsFalse(myAccount.EnterPincode(TestData.ATM_enterPincode_string));
            Assert.AreEqual(myAccount.Deposit(TestData.ATM_deposit_amount), myAccount.Deposit(TestData.ATM_deposit_amount));

        }
        [TestMethod]
        [ExpectedException(typeof(SecurityException))]
        public void MyAccount_EnterPincode_Deposite_zero_return_false()
        {
            Assert.IsFalse(myAccount.EnterPincode(TestData.ATM_enterPincode_string));
            Assert.AreEqual(myAccount.Deposit(TestData.ATM_deposit_amount), 0);
        }
        [TestMethod]
        [ExpectedException(typeof(SecurityException))]
        public void MyAccount_EnterPincode_Deposite_negative_return_false()
        {
            Assert.IsFalse(myAccount.EnterPincode(TestData.ATM_enterPincode_string));
            Assert.AreEqual(myAccount.Deposit(TestData.ATM_deposit_amount_negative), myAccount.Deposit(TestData.ATM_deposit_amount_negative));
        }

        [TestMethod]
        [ExpectedException(typeof(SecurityException))]
        public void MyAccount_EnterPincode_Deposite_100_Withdraw_1000_return_0()
        {
            Assert.IsFalse(myAccount.EnterPincode(TestData.ATM_enterPincode_string));
            myAccount.Deposit(TestData.ATM_deposit_amount);
            myAccount.Withdraw(TestData.ATM_withdraw_amount_return_0);
        }

        [TestMethod]
        [ExpectedException(typeof(SecurityException))]
        public void MyAccount_EnterPincode_Deposite_100_Withdraw_50_return_50()
        {
            Assert.IsFalse(myAccount.EnterPincode(TestData.ATM_enterPincode_string));
            Assert.AreEqual(myAccount.Deposit(TestData.ATM_deposit_amount), myAccount.Withdraw(TestData.ATM_withdraw_amount_return_number));
        }
    }
}
